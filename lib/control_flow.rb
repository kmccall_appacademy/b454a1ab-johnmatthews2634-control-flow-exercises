# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  alphabet = ("a".."z").to_a.join
  str.delete(alphabet)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  idx = str.length / 2
  idx_two = idx - 1
  if str.length.even?
    return str[idx_two..idx]
  end
  str[idx]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  if arr.length == 0
    return ""
  end
  final_array = []
  final_string = ""
  final_array << arr[0]
  arr[1..-1].each do |ele|
    final_array << separator
    final_array << ele
  end
  final_array.each do |x|
    final_string += x.to_s
  end
  final_string

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ""
  str.chars.each_with_index do |letter, idx|
    if (idx + 1).even?
      new_string += letter.upcase
    else
      new_string += letter.downcase
    end
  end
  new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_array = str.split
  final_array = []
  word_array.each do |ele|
    if ele.length >= 5
      final_array << ele.reverse
    else
      final_array << ele
    end
  end
  final_array.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result_array = (1..n).to_a
  result_array.each_with_index do |num, idx|
    if num % 15 == 0
      result_array[idx] = "fizzbuzz"
    elsif num % 3 == 0
      result_array[idx] = "fizz"
    elsif num % 5 == 0
      result_array[idx] = "buzz"
    end
  end
  result_array

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  num_range = (1..num)
  factors_array = []
  num_range.each do |number|
    if num % number == 0
      factors_array << number
    end
  end
  factors_array.length < 3

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  num_range = (1..num)
  factors_array = []
  num_range.each do |number|
    if num % number == 0
      factors_array << number
    end
  end
  factors_array
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  all_factors = factors(num)
  prime_factors = []
  all_factors.each do |factor|
    if prime?(factor)
      prime_factors << factor
    end
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds_arr = []
  evens_arr = []
  arr.each do |num|
    if num.odd?
      odds_arr << num
    else
      evens_arr << num
    end
  end
  
  if odds_arr.length == 1
    odds_arr[0]
  else
    evens_arr[0]
  end
end
